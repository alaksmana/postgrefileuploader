package com.emerio.rnd.cinta.postgrefileuploader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostgrefileuploaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(PostgrefileuploaderApplication.class, args);
	}

}
