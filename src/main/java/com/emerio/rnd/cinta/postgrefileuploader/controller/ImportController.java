package com.emerio.rnd.cinta.postgrefileuploader.controller;

import com.emerio.rnd.cinta.postgrefileuploader.logger.Log;
import com.emerio.rnd.cinta.postgrefileuploader.payload.UploadFileResponse;
import com.emerio.rnd.cinta.postgrefileuploader.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import org.bson.Document;

@RestController
public class ImportController {
    @Autowired
    private ImportService importService;

    @Value("${app.debugenabled}")
    private Boolean debugenabled;

    UploadFileResponse uploadFileresponse = new UploadFileResponse();
    
    @PostMapping(path = "/csv/{screenid}", consumes = "multipart/form-data", produces = "application/json")
    public ResponseEntity<UploadFileResponse> importCsvFile(HttpServletRequest req, @RequestParam("file") MultipartFile file, 
            @PathVariable String screenid) {
        
        try{
                
                Document result = importService.importCsvFileToTableByScreenid(file, screenid);
                if(debugenabled){
                    Log.send(req, "INFO", this.getClass(), Thread.currentThread(),
                    "username", "screenid", "IMPORT FILE", Log.logMessage(req), "returning result = " + result.toJson());
                }

                UploadFileResponse response = new UploadFileResponse();
                    response.setService(this.getClass().getName() + "importCsvFile");
                    response.setData(result);
                if(result.containsKey("error")){
                    response.setMessage("File upload fail!");
                    return ResponseEntity
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response)
                    ;
                }
                else{
                    response.setMessage("File upload successful!");
                    return ResponseEntity
                        .status(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response)
                    ;
                }
                
        } catch (Exception e){
            UploadFileResponse response = new UploadFileResponse();
                response.setMessage("Exception during File upload!");
                response.setService(this.getClass().getName() + "importCsvFile");
                response.setData(e);
            return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response)
            ;
        }
    }
}