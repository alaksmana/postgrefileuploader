package com.emerio.rnd.cinta.postgrefileuploader.dao;

import java.io.File;
import java.sql.Timestamp;
import java.util.List;

import org.bson.Document;

public interface PostgreCustom {
    public Document psqlCreateTable(List<String> columnliststring, List<Document> columnlistdoc, List<Document> pklistdoc, String schemaid, String tableid, Timestamp now);
    public Document psqlInsertTable( String schemaid, String tableid, File csv, Timestamp now);
    public Document psqlInsertTableFromTable(List<String> columnliststring, List<Document> realpklistdoc, String schemaid, String targettableid, String sourcetableid);
    public Document psqlDropTable(String schemaid, String tableid, Timestamp now);
    public Document getPsqlScreenDatasource(String screenid);   
    public Document getPsqlColumnList(String schemaid, String tableid); 
    public Document getPsqlPkListInTable(String schemaid, String tableid);
}