package com.emerio.rnd.cinta.postgrefileuploader.service;

import java.io.File;
import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import com.emerio.rnd.cinta.postgrefileuploader.dao.PostgreCustom;

import org.bson.Document;
import org.springframework.stereotype.Service;

@Service
public class PostgreService{
    @Resource
    PostgreCustom postgreCustom;

    public Document getPsqlColumnList(String schemaid, String tableid){
        return postgreCustom.getPsqlColumnList( schemaid, tableid);
    }
    
    public Document getPsqlPkListInTable(String schemaid, String tableid){
        return postgreCustom.getPsqlPkListInTable(schemaid, tableid);
    }

    public Document psqlCreateTable(List<String> columnliststring, List<Document> columnlistdoc, List<Document> pklistdoc, String schemaid, String tableid, Timestamp now){
        return postgreCustom.psqlCreateTable(columnliststring, columnlistdoc, pklistdoc, schemaid, tableid, now);
    }
    
    public Document psqlInsertTable( String schemaid, String tableid, File csv, Timestamp now){
        return postgreCustom.psqlInsertTable(schemaid, tableid, csv, now);
    }

    public Document psqlInsertTableFromTable(List<String> columnliststring, List<Document> pklistdoc, String schemaid, String targettableid, String sourcetableid){
        return postgreCustom.psqlInsertTableFromTable( columnliststring, pklistdoc, schemaid, targettableid, sourcetableid);
    }

    public Document psqlDropTable(String schemaid, String tableid, Timestamp now){
        return postgreCustom.psqlDropTable( schemaid, tableid, now);
    }

    public Document getPsqlScreenDatasource(String screenid){
        return postgreCustom.getPsqlScreenDatasource(screenid);
    }
}