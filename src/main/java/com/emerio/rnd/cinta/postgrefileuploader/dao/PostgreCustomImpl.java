package com.emerio.rnd.cinta.postgrefileuploader.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.emerio.rnd.cinta.postgrefileuploader.logger.Log;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class PostgreCustomImpl implements PostgreCustom{
    @PersistenceContext
    private EntityManager entityManager;
    @Value("${app.debugenabled}")
    private Boolean debugenabled;

    @Value("${app.configsourceschema}")
    private String configsourceschema;
    @Value("${app.configsourcetable}")
    private String configsourcetable;
    @Value("${app.configsourcecolumn}")
    private String configsourcecolumn;

    @Value("${app.dbhost}")
    private String dbhost;
    @Value("${app.dbport}")
    private String dbport;
    @Value("${spring.datasource.username}")
    private String dbusername;
    @Value("${spring.datasource.password}")
    private String dbpassword;
    @Value("${app.upload-dir}")
    private String uploaddir;

    private String username = "username";
    private String screenid = "screenid";
    public static final String OUTPUT = "output";
    public static final String ERROR = "error";
    public static final String ERRORCODE = "errorcode";
    public static final String PSQL = "psql";
    private static final String BASHHEADER = "#! /bin/bash\n";
    private static final String INFORMATIONSCHEMA_COLUMNNAME = "column_name";
    private static final String INFORMATIONSCHEMA_DATATYPE = "data_type";
    private static final String BASH_DELIMITER = "'\"'\"'";

    private static final String FILEIMPORT = "FILE IMPORT";
    private static final String ERROR_PARSE = "Fail to parse result from psql = ";

    public String psqlCreateScript(String sqlScript, boolean isPsql){
        StringBuilder psqlscript = new StringBuilder();
        psqlscript.append("'");
            psqlscript.append(BASHHEADER);
            if(isPsql){
                psqlscript.append(PSQL).append(" -h ").append(dbhost).append(" -p ").append(dbport).append(" -U ").append(dbusername).append(" -w -t -c ");
                psqlscript.append('"');
                    psqlscript.append(sqlScript);
                psqlscript.append('"');
            }
            else{
                psqlscript.append(sqlScript);
            }
        psqlscript.append("'");

        return psqlscript.toString();
    }

    public String psqlCreateTableScript(List<String> columnliststring, List<Document> columnlistdoc, List<Document> pklistdoc, String schemaid, String tableid, Timestamp now){
        final String currentfunction = "psqlCreateTableScript";
        final String currentmodule = FILEIMPORT;
        StringBuilder sbSqlColumns = new StringBuilder();
        
        sbSqlColumns.append("CREATE TABLE ").append(schemaid).append(".").append(tableid);
        if(now!=null){sbSqlColumns.append(now.getTime());}
        sbSqlColumns.append(" (");
        Iterator<String> iColumns = columnliststring.iterator();

        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "iColumns = " + iColumns.toString());
        }
        while(iColumns.hasNext()){
            String scolumnid = iColumns.next();
            if(debugenabled){
                Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
                username, screenid, currentmodule, currentfunction, "inside loop scolumnid = " + scolumnid);
            }

            sbSqlColumns.append(scolumnid);
            //find column type by columnid
            boolean foundcolumnid = false;
            for(Document columndoc : columnlistdoc){
                String realcolumnid = columndoc.getString(INFORMATIONSCHEMA_COLUMNNAME);
                String realcolumntype = columndoc.getString(INFORMATIONSCHEMA_DATATYPE);
                if(realcolumnid.equals(scolumnid)){
                    sbSqlColumns.append(" ").append(realcolumntype);
                    foundcolumnid = true;
                    break;
                }
            }
            if(!foundcolumnid){
                sbSqlColumns.append(" varchar"); //default
            }
                
            //is pk?
            if(isColumnidExist(scolumnid, pklistdoc)){
                sbSqlColumns.append(" primary key");
            }
            if(iColumns.hasNext()){
                sbSqlColumns.append(",");
            }
        }
        sbSqlColumns.append(");");
        
        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction,  sbSqlColumns.toString());
        }

        //create psql script
        return this.psqlCreateScript(sbSqlColumns.toString(), true);
    }

    @Override
    public Document psqlCreateTable(List<String> columnliststring, List<Document> columnlistdoc, List<Document> pklistdoc, String schemaid, String tableid, Timestamp now){
        
        String psqlscript = "";
        psqlscript = psqlCreateTableScript(columnliststring, columnlistdoc, pklistdoc, schemaid, tableid, now);
                
        Document result = executeProcess( psqlscript, now);
        result.put(PSQL, psqlscript);

        return result;
    }

    public String psqlInsertTableScript(String schemaid, String tableid, String filepath, Timestamp now){
        final String currentfunction = "psqlInsertTableScript";
        final String currentmodule = FILEIMPORT;

        StringBuilder sbSqlColumns = new StringBuilder();
        sbSqlColumns.append('\\').append("copy ").append(schemaid).append(".").append(tableid);
        
        if(null!=now){sbSqlColumns.append(now.getTime());}
        
        sbSqlColumns.append(" FROM ").append(filepath).append(" ");
        sbSqlColumns.append("delimiter ");
        sbSqlColumns.append(BASH_DELIMITER).append(',').append(BASH_DELIMITER).append(" CSV HEADER");
    
        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction,  sbSqlColumns.toString());
        }

        //create psql script
        return this.psqlCreateScript(sbSqlColumns.toString(), true);
    }

    @Override
    public Document psqlInsertTable(String schemaid, String tableid, File csv, Timestamp now){        
        String psqlscript = psqlInsertTableScript(schemaid, tableid, csv.getAbsolutePath(), now);

        Document result = executeProcess( psqlscript, now);
        result.put(PSQL, psqlscript);
            
        return result;
    }

    public Document executeProcess(String psqlscript, Timestamp now){
        final String currentfunction = "executeProcess";
        final String currentmodule = "IMPORT/EXPORT FILE";
        Document resultsource = new Document();

        Map<String,String> envlist = new HashMap<>();
        envlist.put("PGPASSWORD", dbpassword);

        StringBuilder psqlfile = new StringBuilder();
        psqlfile.append(uploaddir).append(File.separator).append(PSQL);
        if(null!=now){psqlfile.append(now.getTime());}
        psqlfile.append(".sh");

        Document resultCreatePsqlFile = this.doProcess(null, "bash", "-c", "echo " + psqlscript + " > " + psqlfile.toString());
        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, resultCreatePsqlFile.toString());
        }
        Document resultChmodPsqlFile = this.doProcess(null, "chmod", "+x", psqlfile.toString());
        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, resultChmodPsqlFile.toString());
        }

        Document result = this.doProcess(envlist, "bash", psqlfile.toString());
        if(result.containsKey(ERROR)){
            resultsource.put(ERROR, result.get(ERROR));
        }
        if(result.containsKey(ERRORCODE)){
            resultsource.put(ERRORCODE, result.get(ERRORCODE));
        }
        if(result.containsKey(OUTPUT)){
            resultsource.put(OUTPUT, result.get(OUTPUT));
        }
        //no need to parse since its just list of string result
        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, result.toString());
        }

        Document resultDeletePsqlFile = this.doProcess(null, "rm", psqlfile.toString());
        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, resultDeletePsqlFile.toString());
        }

        return resultsource;
    }

    private Document doProcess(Map<String,String> envlist, String... command){
        final String currentfunction = "doProcess";
        final String currentmodule = "IMPORT/EXPORT FILE";
        Document result = new Document();
        try {
            
            if(debugenabled){
                Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
                username, screenid, currentmodule, currentfunction, "CMD = "+Arrays.toString(command));
            }
            ProcessBuilder pb = new ProcessBuilder();
            
            Map<String,String> env = pb.environment();
            if(envlist!=null && !envlist.isEmpty())env.putAll(envlist);
            
            pb.command(command);
            Process proc = pb.start();

            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(proc.getInputStream()));

            String line; 
            List<String> listline = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                listline.add(line);
                if(debugenabled){
                    Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
                    username, screenid, currentmodule, currentfunction, "inside loop = "+line);
                }
            }
            

            int iresult = proc.waitFor();
            result.put(ERRORCODE, iresult);
            if(iresult!=0){
                result.put(ERROR, listline);
            }
            else{
                result.put(OUTPUT, listline);
            }
            return result;
        } catch (Exception e) {
            if(debugenabled){
                Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
                username, screenid, currentmodule, currentfunction, "Fail to execute CMD = " + e.toString());
            }
            result.put(ERROR, e.toString());
            result.put(ERRORCODE, -1);
            return result;
        } 
    }

    public String psqlDropTableScript(String schemaid, String tableid, Timestamp now){
        final String currentfunction = "psqlDropTableScript";
        final String currentmodule = FILEIMPORT;

        StringBuilder sbSqlColumns = new StringBuilder();

        sbSqlColumns.append("DROP TABLE IF EXISTS ").append(schemaid).append(".").append('"').append(tableid);
        if(now!=null){sbSqlColumns.append(now.getTime());}
        sbSqlColumns.append('"');

        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction,  sbSqlColumns.toString());
        }

        //create psql script
        return this.psqlCreateScript(sbSqlColumns.toString(), true);
    }

    @Override
    public Document psqlDropTable(String schemaid, String tableid, Timestamp now){

        String psqlscript = psqlDropTableScript( schemaid, tableid, now);

        Document result = executeProcess( psqlscript, now);
        result.put(PSQL, psqlscript);
            
        return result;
    }

    public String psqlInsertTableFromTableScript(List<String> columnliststring, List<Document> realpklistdoc, String schemaid, String targettableid, String sourcetableid){
        final String currentfunction = "psqlInsertTableFromTableScript";
        final String currentmodule = FILEIMPORT;

        StringBuilder sbSqlColumns = new StringBuilder();

        sbSqlColumns.append("INSERT INTO ").append(schemaid).append(".").append(targettableid).append(" (");
            Iterator<String> icolumnlist = columnliststring.iterator();
            while(icolumnlist.hasNext()){
                String columnid = icolumnlist.next();
                sbSqlColumns.append(columnid);
                if(icolumnlist.hasNext()){
                    sbSqlColumns.append(",");
                }
            }
        sbSqlColumns.append(") ");
        sbSqlColumns.append("SELECT ");
            icolumnlist = columnliststring.iterator();
            while(icolumnlist.hasNext()){
                String columnid = icolumnlist.next();
                sbSqlColumns.append("s.").append(columnid);
                if(icolumnlist.hasNext()){
                    sbSqlColumns.append(",");
                }
            }
        sbSqlColumns.append(" FROM ").append(schemaid).append(".").append(sourcetableid).append(" s ");
        sbSqlColumns.append("ON CONFLICT (");
            Iterator<Document> ipklist = realpklistdoc.iterator();
            while(ipklist.hasNext()){
                Document pkdoc = ipklist.next();
                String pkid = pkdoc.getString(INFORMATIONSCHEMA_COLUMNNAME);

                sbSqlColumns.append(pkid);
                if(ipklist.hasNext()){
                    sbSqlColumns.append(",");
                }
            }
        sbSqlColumns.append(") DO UPDATE SET ");
            icolumnlist = columnliststring.iterator();
            while(icolumnlist.hasNext()){
                String columnid = icolumnlist.next();
                if(isColumnidExist(columnid, realpklistdoc)){
                    //skip changing of pkid
                    continue;
                }
                sbSqlColumns.append(columnid).append(" = excluded.").append(columnid);
                if(icolumnlist.hasNext()){
                    sbSqlColumns.append(",");
                }
            }

        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction,  sbSqlColumns.toString());
        }
                
        //create psql script
        return this.psqlCreateScript(sbSqlColumns.toString(), true);
    }
    
    @Override
    public Document psqlInsertTableFromTable(List<String> columnliststring, List<Document> realpklistdoc, String schemaid, String targettableid, String sourcetableid){
        Timestamp now = new Timestamp(System.currentTimeMillis());
        String psqlscript = psqlInsertTableFromTableScript( columnliststring, realpklistdoc, schemaid, targettableid, sourcetableid);

        Document result = executeProcess( psqlscript, now);
        result.put(PSQL, psqlscript);
            
        return result;
    }

    public String getPsqlScreenDatasourceScript(String screenid){
        final String currentfunction = "getPsqlScreenDatasourceScript";
        final String currentmodule = FILEIMPORT;

        StringBuilder sbSqlColumns = new StringBuilder();
        sbSqlColumns.append("SELECT to_json(r) FROM (");
            sbSqlColumns.append("SELECT ").append(configsourcecolumn).append(" FROM ")
            .append(configsourceschema).append('.').append(configsourcetable).append(" WHERE screenid = ").append(BASH_DELIMITER).append(screenid).append(BASH_DELIMITER);
        sbSqlColumns.append(") r");
        
        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction,  sbSqlColumns.toString());
        }

        //create psql script
        return this.psqlCreateScript(sbSqlColumns.toString(), true);
    }

    @Override
    public Document getPsqlScreenDatasource(String screenid){
        Timestamp now = new Timestamp(System.currentTimeMillis());
        String psqlscript = getPsqlScreenDatasourceScript(screenid);
        Document resultsource = new Document();

        Document result = executeProcess( psqlscript, now);
        resultsource.put(PSQL, psqlscript);

        List<Document> doclist = new ArrayList<>();
        try{
            List<String> resultline = (List<String>) result.get(OUTPUT);  
            doclist = convertStringListToDocumentList(resultline);
        }
        catch(Exception e){
            resultsource.put(ERROR, ERROR_PARSE + e.toString());
            return resultsource;
        }

        resultsource.put(OUTPUT, doclist);

        return resultsource;

    }

    public List<Document> convertStringListToDocumentList(List<String> linestoconvert){
        final String currentfunction = "convertStringListToDocumentList";
        final String currentmodule = FILEIMPORT;

            Document templine;
            List<Document> doclist = new ArrayList<>();
            for(String line : linestoconvert){
                if(debugenabled){
                        Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
                        username, screenid, currentmodule, currentfunction, "CONVERTING = " + line);
                }
                if(!line.isEmpty() && !line.isBlank()){
                    templine = Document.parse(line);
                    doclist.add(templine);
                }
            }
            return doclist;
        
    }

    public boolean isColumnidExist(String columnid, List<Document> realcolumndoclist){
        final String currentfunction = "isColumnidExist";
        final String currentmodule = FILEIMPORT;

        try{
            for(Document columndocrow : realcolumndoclist){
                String realcolumnid = columndocrow.getString(INFORMATIONSCHEMA_COLUMNNAME);
                if(realcolumnid.equals(columnid)){
                    return true;
                }
            }
            return false;
        }
        catch(Exception e){
            if(debugenabled){
                Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
                username, "na", currentmodule, currentfunction, "Fail to check columnid against real column doc list = " + e.toString());
            }
            return false;
        }
    }

    public String getPsqlColumnListScript(String schemaid, String tableid) {
        final String currentfunction = "getPsqlColumnListScript";
        final String currentmodule = FILEIMPORT;
        
        StringBuilder sbSqlColumns = new StringBuilder();
        sbSqlColumns.append("SELECT to_json(r) FROM (");
            sbSqlColumns.append("SELECT ");
            sbSqlColumns.append(INFORMATIONSCHEMA_COLUMNNAME).append(", ").append(INFORMATIONSCHEMA_DATATYPE);
            sbSqlColumns.append(" FROM information_schema.columns WHERE table_schema = ").append(BASH_DELIMITER)
            .append(schemaid).append(BASH_DELIMITER).append(" AND table_name = ").append(BASH_DELIMITER).append(tableid).append(BASH_DELIMITER);
        sbSqlColumns.append(") r");
        
        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction,  sbSqlColumns.toString());
        }

        //create psql script
        return this.psqlCreateScript(sbSqlColumns.toString(), true);
    }

    @Override
    public Document getPsqlColumnList(String schemaid, String tableid) {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        String psqlscript = getPsqlColumnListScript(schemaid, tableid);
        Document resultsource = new Document();

        Document result = executeProcess( psqlscript, now);
        
        List<Document> doclist = new ArrayList<>();
        try{
            List<String> resultline = (List<String>) result.get(OUTPUT);  
            doclist = convertStringListToDocumentList(resultline);
        }
        catch(Exception e){
            resultsource.put(ERROR, ERROR_PARSE + e.toString());
            return resultsource;
        }

        resultsource.put(OUTPUT, doclist);
        return resultsource;

    }

    public String getPsqlPkListInTableScript(String schemaid, String tableid) {
        final String currentfunction = "getPsqlPkListInTableScript";
        final String currentmodule = FILEIMPORT;

        StringBuilder sbSqlColumns = new StringBuilder();
        sbSqlColumns.append("SELECT to_json(r) FROM (");
            sbSqlColumns.append("SELECT c.");
            sbSqlColumns.append(INFORMATIONSCHEMA_COLUMNNAME).append(", c.").append(INFORMATIONSCHEMA_DATATYPE);
            sbSqlColumns.append(" FROM information_schema.table_constraints tc ");
            sbSqlColumns.append("JOIN information_schema.constraint_column_usage AS ccu USING ");
            sbSqlColumns.append("(constraint_schema, constraint_name) JOIN information_schema.columns AS c ");
            sbSqlColumns.append("ON c.table_schema = tc.constraint_schema AND tc.table_name = c.table_name ");
            sbSqlColumns.append("AND ccu.column_name = c.column_name WHERE constraint_type = ");
            sbSqlColumns.append(BASH_DELIMITER).append("PRIMARY KEY").append(BASH_DELIMITER).append(" and tc.table_schema = ").append(BASH_DELIMITER);
            sbSqlColumns.append(schemaid).append(BASH_DELIMITER).append(" AND tc.table_name = ").append(BASH_DELIMITER).append(tableid).append(BASH_DELIMITER);
        sbSqlColumns.append(") r");
        
        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction,  sbSqlColumns.toString());
        }

        //create psql script
        return this.psqlCreateScript(sbSqlColumns.toString(), true);
    }

    @Override
    public Document getPsqlPkListInTable(String schemaid, String tableid) {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        String psqlscript = getPsqlPkListInTableScript(schemaid, tableid);
        Document resultsource = new Document();
        Document result = executeProcess( psqlscript, now);
        List<Document> doclist = new ArrayList<>();
        try{
            List<String> resultline = (List<String>) result.get(OUTPUT);  
            doclist = convertStringListToDocumentList(resultline);
        }
        catch(Exception e){
            resultsource.put(ERROR, ERROR_PARSE + e.toString());
            return resultsource;
        }

        resultsource.put(OUTPUT, doclist);
            
        return resultsource;
    }

}
