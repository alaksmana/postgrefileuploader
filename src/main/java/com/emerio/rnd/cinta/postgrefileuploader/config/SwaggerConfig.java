package com.emerio.rnd.cinta.postgrefileuploader.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .paths(Predicates.not(PathSelectors.regex("/error/*")))
                .paths(Predicates.not(PathSelectors.regex("/actuator")))
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        Contact contact = new Contact(
                "NTT-Emerio",
                "https://www.emeriocorp.com/",
                "andre.laksmana@emeriocorp.com");
        return new ApiInfo(
                "PostgreSQL DB uploader",
                "CSV file uploader for PostgreSQL DB using psql utility",
                "@@VERSION@@",
                "https://www.emeriocorp.com/terms-of-use/",
                contact,
                "Copyright@NTT-Emerio",
                "https://www.emeriocorp.com/terms-of-use/",
                Collections.emptyList());
    }
}