package com.emerio.rnd.cinta.postgrefileuploader.service;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import com.emerio.rnd.cinta.postgrefileuploader.logger.Log;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImportService{

    @Autowired
    private PostgreService postgreService;

    @Value("${app.upload-dir}")
    private String uploaddir;

    @Value("${app.debugenabled}")
    private Boolean debugenabled;

    @Value("${app.configsourceschema}")
    private String configsourceschema;
    @Value("${app.configsourcetable}")
    private String configsourcetable;
    @Value("${app.configsourcecolumn}")
    private String configsourcecolumn;

    @Value("${app.dbhost}")
    private String dbhost;
    @Value("${app.dbport}")
    private String dbport;
    @Value("${spring.datasource.username}")
    private String dbusername;
    @Value("${spring.datasource.password}")
    private String dbpassword;

    private String username = "username";
    private static final String OUTPUT = "output";
    private static final String ERROR = "error";
    private static final String ERRORCODE = "errorcode";
    private static final String FILEIMPORT = "FILE IMPORT";

    public Document importCsvFileToTableByScreenid(MultipartFile file, String screenid) {
        final String currentfunction = "importCsvFileToTableByScreenid";
        final String currentmodule = FILEIMPORT;

        Timestamp now = new Timestamp(System.currentTimeMillis());

        Document result = new Document();

        File uploadedfile;
        try{
            uploadedfile = this.saveFile(file, now, screenid);
        }
        catch(Exception e){
            result.put(ERROR, "Fail to save File in the storage = " +e.toString());
            Log.send(null, Log.LOG_ERROR, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Fail to save File in the storage = " +e.toString());
            return result;
        }
        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Saved ok, file path = "+ uploadedfile.getAbsolutePath());
        }
        
        //get column list from file
        List<String> columnliststring = getColumnList(uploadedfile);
        if(Collections.emptyList().equals(columnliststring)){
            result.put(ERROR, "Fail to get list of columns in the file = " + uploadedfile.getAbsolutePath());
            return result;
        }

        //check screenid, get tableid and schemaid from that screenid
        Document screendatasource = this.postgreService.getPsqlScreenDatasource(screenid);
        
        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Returned JSON screendatasource = "+ screendatasource.toString());
        }
        
        String datasourceschematableid = this.getSchemaTableFromScreenConfig(screendatasource);
        if(null==datasourceschematableid){
            result.put(ERROR, "Fail to get schema and table id! Schema in Screen Config is different from config>source>name!");
            return result;
        }
        
        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Parsed source name datasourceschematableid = "+ datasourceschematableid);
        }

        String schemaid = this.getSchemaOrTableFromDatasourceString(datasourceschematableid, true);

        String tableid = this.getSchemaOrTableFromDatasourceString(datasourceschematableid, false);

        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Parsed schema = "+ schemaid);
        }

        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Parsed table = "+ tableid);
        }

        Document realcolumnlist = this.postgreService.getPsqlColumnList(schemaid, tableid);
        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Returned JSON checkcolumnresult = "+ realcolumnlist.toString());
        }

        if(!realcolumnlist.isEmpty() && realcolumnlist.containsKey(ERROR)){
            result.put(ERROR, "Fail to get column list from information_schema = " + realcolumnlist.get(ERROR));
            Log.send(null, Log.LOG_ERROR, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Fail to get column list from information_schema!" + realcolumnlist.get(ERROR));

            return result;
        }
        //list of string of document
        List<Document> realcolumndoclist = new ArrayList<>();
        try{
            realcolumndoclist = (List<Document>) realcolumnlist.get(OUTPUT);
        }
        catch(Exception e){
            result.put(ERROR, "Fail to parse column list from information_schema!" + e.toString());
            Log.send(null, Log.LOG_ERROR, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Fail to parse column list from information_schema!" + e.toString());

            return result;
        }

        //check column list from csv against real column list from table information_schema
        for(String csvcolumnid : columnliststring){
            if(!isColumnidExist(csvcolumnid, realcolumndoclist)){
                result.put(ERROR, "Column id [" + csvcolumnid + "] does not exist in table!");
                Log.send(null, Log.LOG_ERROR, this.getClass(), Thread.currentThread(),
                username, screenid, currentmodule, currentfunction, "Column id [" + csvcolumnid + "] does not exist in table!");
                return result;
            }
        }

        //get PK list in table
        Document pklistdoc = this.postgreService.getPsqlPkListInTable(schemaid, tableid);
        if(!pklistdoc.isEmpty() && pklistdoc.containsKey(ERROR)){
            Log.send(null, Log.LOG_ERROR, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, pklistdoc.getString(ERROR));
            return pklistdoc;
        }

        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Returned JSON pklistdoc = "+ pklistdoc.toString());
        }
        
        //list of string of document
        List<Document> realpklistdoc = new ArrayList<>();
        try{
            realpklistdoc = (List<Document>) pklistdoc.get(OUTPUT);
        }
        catch(Exception e){
            result.put(ERROR, "Fail to parse pk list from information_schema!" + e.toString());
            Log.send(null, Log.LOG_ERROR, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Fail to parse pk list from information_schema!" + e.toString());

            return result;
        }
        if(realpklistdoc.isEmpty()){
            result.put(ERROR, "Table for screen " + screenid + "does not contain PK, please inform your DB admin!");
            Log.send(null, Log.LOG_ERROR, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Table does not contain PK, please inform your DB admin!");

            return result;
        }
        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "PK List = "+ realpklistdoc.toString());
        }
        
        //by now all columns are ok

        //create temp table with columns matching the CSV file
        Document createtableresult = this.postgreService.psqlCreateTable(columnliststring, realcolumndoclist, realpklistdoc, schemaid, tableid, now);

        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Returned JSON createtableresult = "+ createtableresult.toString());
        }
        if(!createtableresult.isEmpty() && createtableresult.containsKey(ERROR)){
            Log.send(null, Log.LOG_ERROR, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, createtableresult.get(ERROR).toString());

            return createtableresult;
        }
        
        //insert csv into temp table
        Document inserttableresult = this.postgreService.psqlInsertTable(schemaid, tableid, uploadedfile, now);

        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Returned JSON inserttableresult = "+ inserttableresult.toString());
        }
        if(!inserttableresult.isEmpty() && inserttableresult.containsKey(ERROR)){
            Log.send(null, Log.LOG_ERROR, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, inserttableresult.get(ERROR).toString());
            return inserttableresult;
        }

        //insert temp table to real table
        Document inserttablefromtableresult = this.postgreService.psqlInsertTableFromTable(columnliststring, realpklistdoc, schemaid, tableid, tableid + now.getTime());

        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Returned JSON inserttablefromtableresult = "+ inserttablefromtableresult.toString());
        }
        if(!inserttablefromtableresult.isEmpty() && inserttablefromtableresult.containsKey(ERROR)){
            Log.send(null, Log.LOG_ERROR, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, inserttablefromtableresult.get(ERROR).toString());
            return inserttablefromtableresult;
        }

        //delete temp table
        Document droptableresult = this.postgreService.psqlDropTable( schemaid, tableid, now);

        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Returned JSON droptableresult = "+ droptableresult.toString());
        }
        if(!droptableresult.isEmpty() && droptableresult.containsKey(ERROR)){
            return droptableresult;
        }
        
        //delete file
        try{
            FileUtils.forceDelete(uploadedfile);
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "Fail to delete uploaded file = "+ uploadedfile.getAbsolutePath());
        }
        catch(Exception e){
            result.put(ERROR, "Fail to delete temporary uploaded file " + uploadedfile.getAbsolutePath() + " = " + e.toString());
            return result;
        }

        if(inserttablefromtableresult.containsKey(OUTPUT)){
            result.put(OUTPUT, inserttablefromtableresult.get(OUTPUT));
        }
        if(inserttablefromtableresult.containsKey(ERROR)){
            result.put(ERROR, inserttablefromtableresult.get(ERROR));
        }
        if(inserttablefromtableresult.containsKey(ERRORCODE)){
            result.put(ERRORCODE, inserttablefromtableresult.get(ERRORCODE));
        }
            

        // get payload before            
        return result;
    }

    public String getFirstLineFromFile(File theFile) throws IOException{
        
        LineIterator it = FileUtils.lineIterator(theFile, "UTF-8");
        String line = "";
        try {
            if (it.hasNext()) {
                line = it.nextLine();
            }
        } finally {
            LineIterator.closeQuietly(it);
        }
        return line;
    }

    public File saveFile(MultipartFile filePart, Timestamp filestamp, String screenid) throws IOException{
        final String currentfunction = "saveFile";
        final String currentmodule = FILEIMPORT;

        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, screenid, currentmodule, currentfunction, "import csv upload file");
        }
        
            String filename = screenid+filestamp.getTime()+".csv";
            //create directory
            new File(uploaddir + File.separator + screenid).mkdirs();

            if(debugenabled){
                Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
                username, screenid, currentmodule, currentfunction, "Filename = " + filename);
            }
            
            File filex = new File(uploaddir+File.separator+screenid+File.separator+filename);
            Path filepath = Paths.get(filex.getPath());

            if(debugenabled){
                Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
                username, screenid, currentmodule, currentfunction, "file path = " + filepath.toString());
            }

            FileUtils.writeByteArrayToFile(filex, filePart.getBytes());
            

        return filex;
    }

    public boolean isColumnidExist(String columnid, List<Document> realcolumndoclist){
        final String currentfunction = "isColumnidExist";
        final String currentmodule = FILEIMPORT;

        try{
            for(Document columndocrow : realcolumndoclist){
                String realcolumnid = columndocrow.getString("column_name");
                if(realcolumnid.equals(columnid)){
                    return true;
                }
            }
            return false;
        }
        catch(Exception e){
            if(debugenabled){
                Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
                username, "na", currentmodule, currentfunction, "Fail to check columnid against real column doc list = " + e.toString());
            }
            return false;
        }
    }

    public List<String> getColumnList(File uploadedfile){
        //get first line of file to get list of columns
        final String currentfunction = "getColumnList";
        final String currentmodule = FILEIMPORT;

        String firstline = "";
        try{
            firstline = this.getFirstLineFromFile(uploadedfile);
        }
        catch(Exception e){
            Log.send(null, Log.LOG_ERROR, this.getClass(), Thread.currentThread(),
            username, "na", currentmodule, currentfunction, "Fail to get first line from the File in the storage = " +e.toString());
            return Collections.emptyList();
        }

        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, "na", currentmodule, currentfunction, "Reading first line = "+ firstline);
        }
        //get list of columns        
        List<CSVRecord> columnlist = new ArrayList<>();
        Reader in = new StringReader(firstline);
        try{
            CSVParser parser = new CSVParser(in, CSVFormat.EXCEL);
            columnlist = parser.getRecords();
            parser.close();
        }
        catch(Exception e){
            Log.send(null, Log.LOG_ERROR, this.getClass(), Thread.currentThread(),
            username, "na", currentmodule, currentfunction, "Fail to parse first line to get the column list = " +e.toString());
            return Collections.emptyList();
        }

        List<String> columnliststring = new ArrayList<>();

        //parse the record to list of string
        for (CSVRecord row : columnlist) {
            Iterator<String> irow = row.iterator();
            irow.forEachRemaining(columnliststring::add);
        }

        if(debugenabled){
            Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
            username, "na", currentmodule, currentfunction, "Parsed first line = "+ columnliststring.toString());
        }
        return columnliststring;
    }

    public String getSchemaTableFromScreenConfig(Document screendatasource){
        final String currentfunction = "getSchemaTableFromScreenConfig";
        final String currentmodule = FILEIMPORT;

        //try parse the result schemaid and tableid
        String datasourceschematableid = "";
        try{
            List<Document> sdoutputlist = (List<Document>)screendatasource.get(OUTPUT);
            //expect 1 row
            Document sdoutput = sdoutputlist.get(0);
            Document sdconfig = (Document)sdoutput.get("config");
            Document sdsource = (Document)sdconfig.get("source");

            if(debugenabled){
                Log.send(null, Log.LOG_INFO, this.getClass(), Thread.currentThread(),
                username, "na", currentmodule, currentfunction, "Returned source = "+ sdsource.toString());
            }

            datasourceschematableid = sdsource.getString("name");
            return datasourceschematableid;
        }
        catch(Exception e){
            
            Log.send(null, Log.LOG_ERROR, this.getClass(), Thread.currentThread(),
            username, "na", currentmodule, currentfunction, "Fail to get schema and table id = " +e.toString());

            return null;
        }
    }

    public String getSchemaOrTableFromDatasourceString(String datasourceschematableid, boolean isSchema){
        //parse schemaid and tableid out of the line
        String schemaid = "";
        StringBuilder sbtableid = new StringBuilder();
        StringTokenizer stdatasourceschematableid = new StringTokenizer(datasourceschematableid, "_");

        boolean firsttoken = true;
        while(stdatasourceschematableid.hasMoreTokens()){
            if(firsttoken){
                schemaid = stdatasourceschematableid.nextToken();
                firsttoken = false;
            }
            else{
                sbtableid.append(stdatasourceschematableid.nextToken());
                if(stdatasourceschematableid.hasMoreTokens()){
                    sbtableid.append("_");
                }
            }
        }
        if(isSchema){
            return schemaid;
        }
        else{
            return sbtableid.toString();
        }
    }
}