package com.emerio.rnd.cinta.postgrefileuploader.logger;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.bson.Document;

public class Log {
    static final Logger logger = LogManager.getLogger();
    static final Marker BALI_TRAILS_MARKER = MarkerManager.getMarker("cinta-audit-trails");
    static final Marker BALI_LOGS_MARKER = MarkerManager.getMarker("cinta-audit-logs");
    public static final String LOG_INFO = "INFO";
    public static final String LOG_WARNING = "WARNING";
    public static final String LOG_ERROR = "ERROR";

    public Log() {
        super();
    }

    public static void send(HttpServletRequest req, String level, Class classname, Thread thread, String user,
            String screenname, String module, String activty, String message) {
        Level lvl = Level.getLevel(level);
        String source = classname.getCanonicalName() + " [" + thread.getStackTrace()[2].getLineNumber() + "]";
        String msg = new Log().message(req, user, screenname, module, activty, source, message);
        logger.printf(lvl, BALI_LOGS_MARKER, msg);
    }

    public static String logMessage(HttpServletRequest req) {
        return "Successfull " + req.getMethod() + " " + req.getRequestURI();
    }

    String message(HttpServletRequest req, String user, String screenname, String module, String activity,
            String source, String message) {
        Document doc = new Document();
        doc.append("accessedby", user);
        doc.append("accessedip", null!=req ? req.getRemoteAddr() : "unknown");
        doc.append("screenname", screenname);
        doc.append("module", module);
        doc.append("activity", activity);
        doc.append("source", source);
        doc.append("logmessage", message);
        return doc.toJson();
    }


}
