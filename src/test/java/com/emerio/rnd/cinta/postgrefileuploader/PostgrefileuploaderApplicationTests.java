package com.emerio.rnd.cinta.postgrefileuploader;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import com.emerio.rnd.cinta.postgrefileuploader.dao.PostgreCustomImpl;
import com.emerio.rnd.cinta.postgrefileuploader.payload.UploadFileResponse;
import com.emerio.rnd.cinta.postgrefileuploader.service.ImportService;
import com.emerio.rnd.cinta.postgrefileuploader.service.PostgreService;

import org.apache.commons.io.FileUtils;
import org.assertj.core.api.Assertions;
import org.bson.Document;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.testcontainers.containers.Container.ExecResult;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.PostgreSQLContainer;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration-test")
@ContextConfiguration(initializers = {PostgrefileuploaderApplicationTests.Initializer.class})

public class PostgrefileuploaderApplicationTests {

	private final String OUTPUT = "output";
	private final String ERROR = "error";

	@Value("${app.configsourceschema}")
    private String configsourceschema;
    @Value("${app.configsourcetable}")
    private String configsourcetable;
    @Value("${app.configsourcecolumn}")
	private String configsourcecolumn;
	@Value("${app.upload-dir}")
    private String uploaddir;

	private static String dbname = "postgres"; //must match with test application properties 
	private static String dbpassword = "my_password";//must match with test application properties 
	private static Integer dbport = 5432;
	private static final String PGPASS = "PGPASSWORD";
	private static final String PSQL = "psql";

	//for testing
	private static final String screenid = "F01"; 
	private static final String screenidcolumn = "screenid";

	@Value("${app.debugenabled}")
    private Boolean debugenabled;

	@ClassRule
	public static Network network = Network.newNetwork();

	@ClassRule
	public static PostgreSQLContainer postgres = (PostgreSQLContainer) new PostgreSQLContainer("postgres:11")
			.withDatabaseName(dbname).withUsername(dbname).withPassword(dbpassword).withExposedPorts(dbport).withNetworkAliases(dbname).withNetwork(network);

	@ClassRule
	public static GenericContainer psqlcontainer = new GenericContainer("192.168.0.51:5000/postgreclijdk:12-jdk-11-cli")
			.withCommand("/bin/sh", "-c", "while true ; do printf 'HTTP/1.1 200 OK\\n\\nyay' | nc -l -p 80 | sleep 10; done")
			.withEnv(PGPASS, dbpassword)
			.withNetwork(network);
	
	@Test
	public void contextLoads() {
		Assertions.assertThat(postgres).isNotNull();
		Assertions.assertThat(psqlcontainer).isNotNull();
		Assertions.assertThat(postgreCustomImpl).isNotNull();
		Assertions.assertThat(importService).isNotNull();
		Assertions.assertThat(postgreService).isNotNull();
	}

	static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=jdbc:tc:postgresql:11://localhost:5432/postgres",
                    "spring.datasource.username=" + postgres.getUsername(),
                    "spring.datasource.password=" + postgres.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
	}
	
	@Autowired
	PostgreService postgreService;
	@Autowired
	ImportService importService;
	@Autowired
	PostgreCustomImpl postgreCustomImpl;
	@Autowired
    private MockMvc mvc;

	private List<String> splitStdOut(String stdout){
		ArrayList<String> resultlist = new ArrayList<>();
		StringTokenizer stdouttoken = new StringTokenizer(stdout, "\n");
		while(stdouttoken.hasMoreTokens()){
			String row = stdouttoken.nextToken();
			if(null!=row && !row.isBlank()){
				resultlist.add(row);
			}
		}
		return resultlist;
	}

	private ExecResult executeProcess(String psqlscript, Timestamp now){
        Map<String,String> envlist = new HashMap<>();
        envlist.put(PGPASS, dbpassword);

        StringBuilder psqlfile = new StringBuilder();
        psqlfile.append(uploaddir).append(File.separator).append(PSQL);
        if(null!=now){psqlfile.append(now.getTime());}
		psqlfile.append(".sh");
		ExecResult result = null;
		try{
			ExecResult resultCreatePsqlFile = psqlcontainer.execInContainer("bash", "-c", "echo " + psqlscript + " > " + psqlfile.toString());
			if(debugenabled){
				System.out.println("@@@@@@ resultCreatePsqlFile="+resultCreatePsqlFile.toString());
			}
			ExecResult resultChmodPsqlFile = psqlcontainer.execInContainer("chmod", "+x", psqlfile.toString());
			if(debugenabled){
				System.out.println("@@@@@@ resultChmodPsqlFile="+resultChmodPsqlFile.toString());
			}

			result = psqlcontainer.execInContainer("bash", psqlfile.toString());
			if(debugenabled){
				System.out.println("@@@@@@ result="+result.toString());
			}
			
			ExecResult resultDeletePsqlFile = psqlcontainer.execInContainer("rm", psqlfile.toString());
			if(debugenabled){
				System.out.println("@@@@@@ resultDeletePsqlFile="+resultDeletePsqlFile.toString());
			}
			
		}
		catch(Exception e){
			if(debugenabled){
				System.out.println("@@@@@@ Exception during execute process="+e.toString());
			}
		}
		return result;
	}

	private String generateTestcsv1(){
		// create csv file
		StringBuilder sbTestcsv = new StringBuilder();
		sbTestcsv.append("'");
		sbTestcsv.append("id,kolom1,kolom2,").append(screenidcolumn).append(",").append(configsourcecolumn).append('\n');
		sbTestcsv.append("1,value1,4,").append(screenid).append(",");
			sbTestcsv.append("\"{ \"\"config\"\": { \"\"source\"\": { \"\"name\"\": \"\"").append(configsourceschema).append('_').append(configsourcetable).append("\"\"}}}\"");

		sbTestcsv.append("'");

		return sbTestcsv.toString();
	}

	private String generateTestcsv2(){
		StringBuilder sbTestcsvNow = new StringBuilder();
		sbTestcsvNow.append("'");
		sbTestcsvNow.append("id,kolom1,kolom2,").append(screenidcolumn).append(",").append(configsourcecolumn).append('\n');
			sbTestcsvNow.append("2,value2,8,").append("F02").append(",");
		sbTestcsvNow.append("\"{ \"\"config\"\": { \"\"source\"\": { \"\"name\"\": \"\"").append(configsourceschema).append('_').append(configsourcetable).append("\"\"}}}\"");

		sbTestcsvNow.append("'");

		return sbTestcsvNow.toString();
	}

	@Test
	public void testUploadFileResponse(){
		String message = "test message";
		String service = "test service";
		String data = "test data";
		UploadFileResponse testResponse = new UploadFileResponse();
		testResponse.setMessage(message);
		testResponse.setService(service);
		testResponse.setData(data);
		Assertions.assertThat(testResponse.getMessage().equals(message)).isTrue();
		Assertions.assertThat(testResponse.getService().equals(service)).isTrue();
		Assertions.assertThat(testResponse.getData().equals(data)).isTrue();
	}
	
	@Test
	public void testExecuteProcess(){
		Timestamp now = new Timestamp(System.currentTimeMillis());
		//create echo test
		String bashscript = this.postgreCustomImpl.psqlCreateScript("echo 1234567", false);
			
		Document result = this.postgreCustomImpl.executeProcess(bashscript, now);
		if(debugenabled){
			System.out.println("@@@@@ bashscript= " + bashscript);
			System.out.println("@@@@@ testExecuteProcess= " + result.toJson());
		}
			
		Assertions.assertThat(result.get(OUTPUT).toString().contains("1234567")).isTrue();
	}

	@Test
	public void testFailExecuteProcess(){
		Timestamp now = null;
		//create echo test
		String bashscript = this.postgreCustomImpl.psqlCreateScript("dunno xxxx", false);
			
		Document result = this.postgreCustomImpl.executeProcess(bashscript, now);
		if(debugenabled){
			System.out.println("@@@@@ bashscript= " + bashscript);
			System.out.println("@@@@@ testFailExecuteProcess= " + result.toJson());
		}
			
		Assertions.assertThat(result.containsKey(ERROR)).isTrue();

	}

	@Test
	public void testFileCsv(){
		List<String> columnliststring = Arrays.asList("id", "kolom1", "kolom2", "kolom3");
		StringBuilder sbTestcsv = new StringBuilder();
		sbTestcsv.append("id,kolom1,kolom2,kolom3").append('\n');
		sbTestcsv.append("1,value1,4,");
			sbTestcsv.append("\"{ \"\"config\"\": { \"\"source\"\": { \"\"name\"\": \"\"").append(configsourceschema).append('_').append(configsourcetable).append("\"\"}}}\"");

		File test = new File("/tmp/xtestx.csv");
		try {
			FileUtils.writeStringToFile(test, sbTestcsv.toString());
			List<String> realcolumnlist = this.importService.getColumnList(test);
			if(debugenabled){
				System.out.println("@@@@@ Column list = " + realcolumnlist.toString());
			}
			Assertions.assertThat(realcolumnlist.equals(columnliststring)).isTrue();
			
			List<String> negativetest = this.importService.getColumnList(null);
			Assertions.assertThat(negativetest.equals(Collections.emptyList())).isTrue();
		} catch (Exception e) {
			System.out.println("@@@@@ Exception during creation of CSV file for testing! = " + e.toString());
			Assertions.assertThat(false).isTrue();
		}
			
	}

	@Test
	public void testUploadFile(){
		Timestamp now = new Timestamp(System.currentTimeMillis());
		MockMultipartFile multipartFile = new MockMultipartFile("file", "testupload1.csv",
				"text/plain", this.generateTestcsv1().getBytes());
		try {
			File uploadedfile = this.importService.saveFile(multipartFile, now, screenid);
			Assertions.assertThat(uploadedfile.getAbsolutePath().toString().startsWith(uploaddir)).isTrue();
		} catch (Exception e) {
			System.out.println("@@@@@ Exception during uploading of CSV file for testing! = " + e.toString());
			Assertions.assertThat(false).isTrue();
		}
	}

	@Test
	public void testFailUploadService(){
		MockMultipartFile multipartFile = new MockMultipartFile("file", "testupload2.csv",
				"text/plain", this.generateTestcsv2().getBytes());
		
		try {
			ResultActions resultServiceUpload = this.mvc.perform(multipart("/csv/" + screenid).file(multipartFile))
				.andExpect(status().is5xxServerError());
			if(debugenabled){
				System.out.println("@@@@@@@@@@@ testFailUploadService = " + resultServiceUpload.andReturn().getResponse().getContentAsString());
			}
		} catch (Exception e) {
			System.out.println("@@@@@ Exception during multipart controller uploading of CSV file for testing! = " + e.toString());
			Assertions.assertThat(false).isTrue();
		}
		
	}

	@Test
	public void testExceptionUploadService(){
		MockMultipartFile multipartFile = new MockMultipartFile("file", "testupload2.csv",
				"text/plain", this.generateTestcsv2().getBytes());
		
		try {
			ResultActions resultServiceUpload = this.mvc.perform(multipart("/csv/").file(multipartFile))
				.andExpect(status().is4xxClientError());
			if(debugenabled){
				System.out.println("@@@@@@@@@@@ testExceptionUploadService = " + resultServiceUpload.andReturn().getResponse().getContentAsString());
			}
		} catch (Exception e) {
			System.out.println("@@@@@ Exception during testExceptionUploadService! = " + e.toString());
			Assertions.assertThat(false).isTrue();
		}
		
	}

	@Test
	public void testFailGetColumnList(){
		Document realcolumnlist = this.postgreService.getPsqlColumnList(configsourceschema, configsourcetable);
		Assertions.assertThat(realcolumnlist.containsKey(ERROR)).isTrue();
		if(debugenabled){
			System.out.println("@@@@@@@@@@@ testFailGetColumnList = " + realcolumnlist.toJson());
		}
	}

	@Test
	public void testFailGetPkList(){
		Document pklistdoc = this.postgreService.getPsqlPkListInTable(configsourceschema, configsourcetable);
		Assertions.assertThat(pklistdoc.containsKey(ERROR)).isTrue();
		if(debugenabled){
			System.out.println("@@@@@@@@@@@ testFailGetPkList = " + pklistdoc.toJson());
		}
	}
	
	@Test
	public void testFailInsertTable(){
		Timestamp now = new Timestamp(System.currentTimeMillis());
		List<String> columnliststring = Arrays.asList("id", "kolom1", "kolom2", screenidcolumn, configsourcecolumn);
		List<String> columntypestring = Arrays.asList("bigint", "text", "int", "text", "jsonb");

		List<String> pkliststring = Arrays.asList("id");
		List<String> pktypestring = Arrays.asList("bigint");
		

		List<Document> realcolumnlistdoc = new ArrayList<>();
		List<Document> pklistdoc = new ArrayList<>();

		//generate realcolumndoclist
		for(int i=0; i<columnliststring.size(); i++){
			String columnid = columnliststring.get(i);
			String columntype = columntypestring.get(i);

			Document columndoc = new Document();
			columndoc.put("column_name", columnid);
			columndoc.put("data_type", columntype);

			realcolumnlistdoc.add(columndoc);
		}
		//generate pkdoclist
		for(int i=0; i<pkliststring.size(); i++){
			String columnid = pkliststring.get(i);
			String columntype = pktypestring.get(i);

			Document pkdoc = new Document();
			pkdoc.put("column_name", columnid);
			pkdoc.put("data_type", columntype);

			pklistdoc.add(pkdoc);
		}

		MockMultipartFile multipartFile = new MockMultipartFile("file", "testupload1.csv",
				"text/plain", this.generateTestcsv1().getBytes());
		try {
			File uploadedfile = this.importService.saveFile(multipartFile, now, screenid);
			Assertions.assertThat(uploadedfile.getAbsolutePath().toString().startsWith(uploaddir)).isTrue();
			Document inserttableresult = this.postgreService.psqlInsertTable(configsourceschema, configsourcetable, uploadedfile, now);
			Assertions.assertThat(inserttableresult.containsKey(ERROR)).isTrue();

			Document inserttablefromtableresult = this.postgreService.psqlInsertTableFromTable(columnliststring, pklistdoc, configsourceschema, configsourcetable, configsourcetable + now.getTime());
			if(debugenabled){
				System.out.println("@@@@@@@@@@@ testFailInsertTable = " + inserttableresult.toJson());
				System.out.println("@@@@@@@@@@@ inserttablefromtableresult = " + inserttablefromtableresult.toJson());
			}

		} catch (Exception e) {
			System.out.println("@@@@@ Exception during testFailInsertTable! = " + e.toString());
			Assertions.assertThat(false).isTrue();
		}
	}

	@Test
	public void testFailDropTable(){
		Timestamp now = new Timestamp(System.currentTimeMillis());

		Document droptableresult = this.postgreService.psqlDropTable( configsourceschema, configsourcetable, now);
		Assertions.assertThat(droptableresult.containsKey(ERROR)).isTrue();
		if(debugenabled){
			System.out.println("@@@@@@@@@@@ testFailDropTable = " + droptableresult.toJson());
		}
	}

	@Test
	public void testFailCreateTable(){
		Timestamp now = new Timestamp(System.currentTimeMillis());
		
		List<String> columnliststring = Arrays.asList("id", "kolom1", "kolom2", screenidcolumn, configsourcecolumn);
		List<String> columntypestring = Arrays.asList("bigint", "text", "int", "text", "jsonb");

		List<String> pkliststring = Arrays.asList("id");
		List<String> pktypestring = Arrays.asList("bigint");
		

		List<Document> realcolumnlistdoc = new ArrayList<>();
		List<Document> pklistdoc = new ArrayList<>();

		//generate realcolumndoclist
		for(int i=0; i<columnliststring.size(); i++){
			String columnid = columnliststring.get(i);
			String columntype = columntypestring.get(i);

			Document columndoc = new Document();
			columndoc.put("column_name", columnid);
			columndoc.put("data_type", columntype);

			realcolumnlistdoc.add(columndoc);
		}
		//generate pkdoclist
		for(int i=0; i<pkliststring.size(); i++){
			String columnid = pkliststring.get(i);
			String columntype = pktypestring.get(i);

			Document pkdoc = new Document();
			pkdoc.put("column_name", columnid);
			pkdoc.put("data_type", columntype);

			pklistdoc.add(pkdoc);
		}
		Document createtableresult = this.postgreService.psqlCreateTable(columnliststring, realcolumnlistdoc, pklistdoc, configsourceschema, configsourcetable, now);
		Assertions.assertThat(createtableresult.containsKey(ERROR)).isTrue();
		if(debugenabled){
			System.out.println("@@@@@@@@@@@ testFailCreateTable = " + createtableresult.toJson());
		}
	}

	@Test
	public void testFailIsColumnIdExist(){
		//negative test
		ArrayList<Document> negativedoclist = new ArrayList<>();
		Document negativedoc = new Document();
		negativedoc.put("xxx", "Hahaha");
		negativedoclist.add(negativedoc);

		Assertions.assertThat(this.importService.isColumnidExist("kolom1", negativedoclist)).isFalse(); 

	}

	@Test
	public void createTableCheckPkAndColumnsThenDeleteTable(){
		Timestamp now = new Timestamp(System.currentTimeMillis());
		
		List<String> columnliststring = Arrays.asList("id", "kolom1", "kolom2", screenidcolumn, configsourcecolumn);
		List<String> columntypestring = Arrays.asList("bigint", "text", "int", "text", "jsonb");
		List<String> negativecolumnliststring = Arrays.asList("w", "x", "y", "z"); 

		List<String> pkliststring = Arrays.asList("id");
		List<String> pktypestring = Arrays.asList("bigint");
		

		List<Document> realcolumnlistdoc = new ArrayList<>();
		List<Document> pklistdoc = new ArrayList<>();

		//generate realcolumndoclist
		for(int i=0; i<columnliststring.size(); i++){
			String columnid = columnliststring.get(i);
			String columntype = columntypestring.get(i);

			Document columndoc = new Document();
			columndoc.put("column_name", columnid);
			columndoc.put("data_type", columntype);

			realcolumnlistdoc.add(columndoc);
		}
		//generate pkdoclist
		for(int i=0; i<pkliststring.size(); i++){
			String columnid = pkliststring.get(i);
			String columntype = pktypestring.get(i);

			Document pkdoc = new Document();
			pkdoc.put("column_name", columnid);
			pkdoc.put("data_type", columntype);

			pklistdoc.add(pkdoc);
		}
		
		//create schema quickly with container				
		StringBuilder sSql = new StringBuilder();					
			sSql.append("CREATE SCHEMA IF NOT EXISTS ").append(configsourceschema).append(";");
		String psqlscript = postgreCustomImpl.psqlCreateScript(sSql.toString(), true);
		ExecResult resultSchema = this.executeProcess(psqlscript, now);
		Assertions.assertThat(resultSchema!=null && resultSchema.getExitCode()==0).isTrue();

		//create table
		String createtablescript = this.postgreCustomImpl.psqlCreateTableScript(columnliststring, realcolumnlistdoc, pklistdoc, configsourceschema, configsourcetable, null);
		ExecResult resultCreateTable = this.executeProcess(createtablescript, now);
		Assertions.assertThat(resultCreateTable.getExitCode()==0).isTrue();

		String createtablescriptwtime = this.postgreCustomImpl.psqlCreateTableScript(columnliststring, realcolumnlistdoc, pklistdoc, configsourceschema, configsourcetable, now);
		ExecResult resultCreateTablewtime = this.executeProcess(createtablescriptwtime, now);
		Assertions.assertThat(resultCreateTablewtime.getExitCode()==0).isTrue();

		// //check columns created matching?
		String realcolumnscript = this.postgreCustomImpl.getPsqlColumnListScript(configsourceschema, configsourcetable);
		ExecResult resultGetColumns = this.executeProcess(realcolumnscript, now);
		Assertions.assertThat(resultGetColumns.getExitCode()==0).isTrue();
		if(debugenabled){
			System.out.println("@@@@@ bashscript= " + realcolumnscript);
			System.out.println("@@@@@ resultGetColumns= " + resultGetColumns.toString());
		}
		// //check columns created matching? wtime
		String realcolumnscriptwtime = this.postgreCustomImpl.getPsqlColumnListScript(configsourceschema, configsourcetable + now.getTime());
		ExecResult resultGetColumnswtime = this.executeProcess(realcolumnscriptwtime, now);
		Assertions.assertThat(resultGetColumnswtime.getExitCode()==0).isTrue();
		if(debugenabled){
			System.out.println("@@@@@ bashscript= " + realcolumnscriptwtime);
			System.out.println("@@@@@ resultGetColumnswtime= " + resultGetColumnswtime.toString());
		}
		
		List<Document> realcolumndoclist = this.postgreCustomImpl.convertStringListToDocumentList(this.splitStdOut(resultGetColumns.getStdout()));
		
        //check column list from csv against real column list from table information_schema
        for(String csvcolumnid : columnliststring){
            try{
				Assertions.assertThat(this.postgreCustomImpl.isColumnidExist(csvcolumnid, realcolumndoclist)).isTrue(); 
				Assertions.assertThat(this.importService.isColumnidExist(csvcolumnid, realcolumndoclist)).isTrue(); 
			}
			catch(Exception e){
				System.out.println("@@@@@@@@ Exception in checking column list!  = " +e.toString());
				Assertions.assertThat(false).isTrue(); //exception
			}			
		}
		
		for(String csvcolumnid : negativecolumnliststring){
            try{
				Assertions.assertThat(this.postgreCustomImpl.isColumnidExist(csvcolumnid, realcolumndoclist)).isFalse(); 
				Assertions.assertThat(this.importService.isColumnidExist(csvcolumnid, realcolumndoclist)).isFalse(); 
			}
			catch(Exception e){
				System.out.println("@@@@@@@@ Exception in checking negative column list!  = " +e.toString());
				Assertions.assertThat(false).isTrue(); //exception
			}			
		}
		
		//check primary key
		String pklistscript = this.postgreCustomImpl.getPsqlPkListInTableScript( configsourceschema, configsourcetable);
		ExecResult resultPklistscript = this.executeProcess(pklistscript, now);
		Assertions.assertThat(resultPklistscript.getExitCode()==0).isTrue();
		if(debugenabled){
			System.out.println("@@@@@ bashscript= " + resultPklistscript);
			System.out.println("@@@@@ resultPklistscript= " + resultPklistscript.toString());
		}
		
		List<Document> realpkdoclist = this.postgreCustomImpl.convertStringListToDocumentList(this.splitStdOut(resultPklistscript.getStdout()));

		for(String pkid : pkliststring){
			try{
				Assertions.assertThat(this.postgreCustomImpl.isColumnidExist(pkid, realpkdoclist)).isTrue(); 
			}
			catch(Exception e){
				System.out.println("@@@@@@@@ Exception in checking pk list!  = " +e.toString());
				Assertions.assertThat(false).isTrue(); //exception
			}	
		}

		//generate csv
		// create csv file
		String sbTestcsv = this.generateTestcsv1();
		
		//generate the file in container
		try{
			psqlcontainer.execInContainer("bash", "-c", "echo " + sbTestcsv + " > " + "/tmp/test.csv");
		}
		catch(Exception e){
			System.out.println("@@@@@ Exception during creation of CSV file = " + e.toString());
			Assertions.assertThat(false).isTrue();
		}			

		//insert table with file
		String inserttablescript = this.postgreCustomImpl.psqlInsertTableScript(configsourceschema, configsourcetable, "/tmp/test.csv", null);
		ExecResult resultInserttablescript = this.executeProcess(inserttablescript, now);
		Assertions.assertThat(resultInserttablescript.getExitCode()==0).isTrue();
		Assertions.assertThat(resultInserttablescript.getStdout().contains("COPY 1")).isTrue(); //insert 1 row only
		if(debugenabled){
			System.out.println("@@@@@ bashscript= " + inserttablescript);
			System.out.println("@@@@@ resultInserttablescript= " + resultInserttablescript.toString());
		}

		//check screenid, get tableid and schemaid from that screenid
		String getscreenidscript = this.postgreCustomImpl.getPsqlScreenDatasourceScript(screenid);
		ExecResult resultGetscreenidscript = this.executeProcess(getscreenidscript, now);
		Assertions.assertThat(resultGetscreenidscript.getExitCode()==0).isTrue();
		
		if(debugenabled){
			System.out.println("@@@@@ bashscript= " + getscreenidscript);
			System.out.println("@@@@@ resultGetscreenidscript= " + resultGetscreenidscript.toString());
		}

		// resultGetscreenidscript
		List<Document> screeniddoclist = this.postgreCustomImpl.convertStringListToDocumentList(this.splitStdOut(resultGetscreenidscript.getStdout()));
		Assertions.assertThat(null!=screeniddoclist && !screeniddoclist.isEmpty()).isTrue();
		Document screendatasource = screeniddoclist.get(0);
		
		ArrayList<Document> screendatasourcedoclist = new ArrayList<>();
		Document configscreendatasource = (Document) screendatasource.get("config");
		screendatasourcedoclist.add(configscreendatasource);
		Document configwrapper = new Document();
		configwrapper.put(OUTPUT, screendatasourcedoclist);

		String datasourceschematableid = this.importService.getSchemaTableFromScreenConfig(configwrapper);
		if(debugenabled){
			System.out.println("@@@@@ YOUR SCHEMA FOR SCREENCONFIG MUST BE = " + screeniddoclist.toString());			
			System.out.println("@@@@@ datasourceschematableid= " + datasourceschematableid);
		}
		Assertions.assertThat(datasourceschematableid.equals("dev_screen_config")).isTrue();
        
		String schemaid = this.importService.getSchemaOrTableFromDatasourceString(datasourceschematableid, true);
		Assertions.assertThat(schemaid.equalsIgnoreCase(configsourceschema)).isTrue();

		String tableid = this.importService.getSchemaOrTableFromDatasourceString(datasourceschematableid, false);
		Assertions.assertThat(tableid.equalsIgnoreCase(configsourcetable)).isTrue();

		//insert table from table
		//generate csv + now
		// create csv + now file
		String sbTestcsvNow = generateTestcsv2();
		
		//generate the file in container
		try{
			psqlcontainer.execInContainer("bash", "-c", "echo " + sbTestcsvNow + " > " + "/tmp/test" + now.getTime() + ".csv");
		}
		catch(Exception e){
			System.out.println("@@@@@ Exception during creation of CSV file = " + e.toString());
			Assertions.assertThat(false).isTrue();
		}			

		//insert table with file
		String inserttablenowscript = this.postgreCustomImpl.psqlInsertTableScript(configsourceschema, configsourcetable, "/tmp/test" + now.getTime() + ".csv", null);
		ExecResult resultInserttablenowscript = this.executeProcess(inserttablenowscript, now);
		Assertions.assertThat(resultInserttablenowscript.getExitCode()==0).isTrue();
		Assertions.assertThat(resultInserttablenowscript.getStdout().contains("COPY 1")).isTrue(); //insert 1 row only
		if(debugenabled){
			System.out.println("@@@@@ bashscript= " + inserttablenowscript);
			System.out.println("@@@@@ resultInserttablenowscript= " + resultInserttablenowscript.toString());
		}

		//insert table from table
		String inserttablefromtablescript = this.postgreCustomImpl.psqlInsertTableFromTableScript(columnliststring, realpkdoclist, schemaid, tableid, tableid + now.getTime());
		ExecResult resultInserttablefromtablescript = this.executeProcess(inserttablefromtablescript, now);
		Assertions.assertThat(resultInserttablefromtablescript.getExitCode()==0).isTrue();
		// Assertions.assertThat(resultInserttablefromtablescript.getStdout().contains("COPY 1")).isTrue(); //insert 1 row only
		if(debugenabled){
			System.out.println("@@@@@ bashscript= " + inserttablefromtablescript);
			System.out.println("@@@@@ resultInserttablefromtablescript= " + resultInserttablefromtablescript.toString());
		}

		//check screenid, get tableid and schemaid from that screenid
		String getscreenidscriptf02 = this.postgreCustomImpl.getPsqlScreenDatasourceScript("F02");
		ExecResult resultGetscreenidscriptf02 = this.executeProcess(getscreenidscriptf02, now);
		Assertions.assertThat(resultGetscreenidscriptf02.getExitCode()==0).isTrue();
		
		if(debugenabled){
			System.out.println("@@@@@ bashscript= " + getscreenidscriptf02);
			System.out.println("@@@@@ resultGetscreenidscriptf02= " + resultGetscreenidscriptf02.toString());
		}

		// resultGetscreenidscript
		List<Document> f02doclist = this.postgreCustomImpl.convertStringListToDocumentList(this.splitStdOut(resultGetscreenidscriptf02.getStdout()));
		Assertions.assertThat(null!=f02doclist && !f02doclist.isEmpty()).isTrue();
		Document f02datasource = f02doclist.get(0);
		
		ArrayList<Document> f02datasourcedoclist = new ArrayList<>();
		Document configf02datasource = (Document) f02datasource.get("config");
		f02datasourcedoclist.add(configf02datasource);
		Document f02configwrapper = new Document();
		f02configwrapper.put(OUTPUT, f02datasourcedoclist);

		String f02datasourceschematableid = this.importService.getSchemaTableFromScreenConfig(f02configwrapper);
		if(debugenabled){
			System.out.println("@@@@@ YOUR SCHEMA FOR SCREENCONFIG MUST BE = " + f02doclist.toString());			
			System.out.println("@@@@@ f02datasourceschematableid= " + f02datasourceschematableid);
		}
		Assertions.assertThat(f02datasourceschematableid.equals("dev_screen_config")).isTrue();
        
		String f02schemaid = this.importService.getSchemaOrTableFromDatasourceString(f02datasourceschematableid, true);
		Assertions.assertThat(f02schemaid.equalsIgnoreCase(configsourceschema)).isTrue();

		String f02tableid = this.importService.getSchemaOrTableFromDatasourceString(f02datasourceschematableid, false);
		Assertions.assertThat(f02tableid.equalsIgnoreCase(configsourcetable)).isTrue();

		//delete the table 
		String deletetablescript = this.postgreCustomImpl.psqlDropTableScript(configsourceschema, configsourcetable, null);
		ExecResult resultDeletetablescript = this.executeProcess(deletetablescript, now);
		Assertions.assertThat(resultDeletetablescript.getExitCode()==0).isTrue();
		Assertions.assertThat(resultDeletetablescript.getStdout().contains("DROP TABLE")).isTrue(); //insert 1 row only
		if(debugenabled){
			System.out.println("@@@@@ bashscript= " + deletetablescript);
			System.out.println("@@@@@ resultDeletetablescript= " + resultDeletetablescript.toString());
		}

		//delete the table now 
		String deletetablenowscript = this.postgreCustomImpl.psqlDropTableScript(configsourceschema, configsourcetable+now.getTime(), null);
		ExecResult resultDeletetablenowscript = this.executeProcess(deletetablenowscript, now);
		Assertions.assertThat(resultDeletetablenowscript.getExitCode()==0).isTrue();
		Assertions.assertThat(resultDeletetablenowscript.getStdout().contains("DROP TABLE")).isTrue(); //insert 1 row only
		if(debugenabled){
			System.out.println("@@@@@ bashscript= " + deletetablenowscript);
			System.out.println("@@@@@ resultDeletetablenowscript= " + resultDeletetablenowscript.toString());
		}
	}

}
