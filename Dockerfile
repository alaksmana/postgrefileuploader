# emerioindonesia/postgreclijdk:12-jdk-11-cli is from below:
# FROM openjdk:12-jdk
# RUN yum install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
# RUN yum install postgresql11

FROM 192.168.0.51:5000/postgreclijdk:12-jdk-11-cli
ADD ./target/postgrefileuploader-@@VERSION@@.jar postgrefileuploader.jar
ADD ./postgrefileuploader.sh postgrefileuploader.sh
RUN ["chmod", "+x", "/postgrefileuploader.sh"]
ENTRYPOINT ["/postgrefileuploader.sh"]
